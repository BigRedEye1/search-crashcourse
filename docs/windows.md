# Настройка окружения на Windows

## CMake
В первую очередь установите CMake: https://cmake.org/download/.
После чего попробуйте собрать в консоли так, как написано в [setup.md](/docs/setup.md).

Код писать проще всего в CLion или VSCode с плагинами для CMake. Другие IDE так же умеют открывать CMake проекты (MSVC).

## MSYS2
Если нативный CMake не завелся по разным причинам, то попробуйте использовать MSYS2. Это более сложный альтернативный механизм.
1. Установите MSYS2: https://www.msys2.org/
1. В терминале MSYS2 выполните `pacman -S mingw-w64-clang-x86_64-toolchain`
1. В терминале MSYS2 выполните `pacman -S mingw-w64-cmake`
1. Перейдите в терминале MSYS2 в директорию с проектом (команда cd)
1. Собирайте через CMake, как описано в [setup.md](/docs/setup.md).

## Ничего не работает
Пишите https://t.me/BigRedEye в личку.
