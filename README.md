# Search crashcourse

* [Как начать работу с курсом](docs/setup.md)
* [Решение распространённых проблем](docs/troubleshooting.md)
* [Веб-интерфейс с результатами проверки](https://sirius.cpp-hse.net)
