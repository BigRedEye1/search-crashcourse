#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "../../contrib/doctest/doctest/doctest.h"

#include "index.h"


TEST_SUITE("Index building") {
    TEST_CASE("Does not fail") {
        codesearch::IndexBuilder builder{codesearch::IndexOptions{
            .arity = 3,
        }};

        builder.AddDocument(codesearch::Document{
            .path = "aboba",
            .text = "abracadabra"
        });

        REQUIRE_NOTHROW(std::move(builder).Finish());
    }

    TEST_CASE("Indexes documents") {
        codesearch::IndexBuilder builder{codesearch::IndexOptions{
            .arity = 3,
        }};

        builder.AddDocument(codesearch::Document{
            .path = "aboba",
            .text = "abracadabra",
        });
        builder.AddDocument(codesearch::Document{
            .path = "codesearch",
            .text = "Search is love. Search is life. Search for strings!",
        });

        codesearch::Index index = std::move(builder).Finish();

        REQUIRE_EQ(index.DocumentCount(), 2);

        SUBCASE("Document contents") {
            const codesearch::IndexedDocument* doc = nullptr;
            REQUIRE_NOTHROW(doc = &index.GetDocument(0));
            REQUIRE_EQ(doc->id, 0);
            REQUIRE_EQ(doc->path, "aboba");
            REQUIRE_EQ(doc->text, "abracadabra");

            REQUIRE_NOTHROW(doc = &index.GetDocument(1));
            REQUIRE_EQ(doc->id, 1);
            REQUIRE_EQ(doc->path, "codesearch");
            REQUIRE_EQ(doc->text, "Search is love. Search is life. Search for strings!");
        }
    }

    TEST_CASE("Builds inverted index") {
        codesearch::IndexBuilder builder{codesearch::IndexOptions{
            .arity = 3,
        }};

        builder.AddDocument(codesearch::Document{
            .path = "aboba",
            .text = "abracadabring",
        });
        builder.AddDocument(codesearch::Document{
            .path = "codesearch",
            .text = "Search is love. Search is life. Search for strings!",
        });
        builder.AddDocument(codesearch::Document{
            .path = "a.txt",
            .text = "aaaaaaaaaaaaaaaaaaaaaa",
        });

        codesearch::Index index = std::move(builder).Finish();

        SUBCASE("Inverted index gut contents") {
            auto check = [&index](const char* name, const std::vector<codesearch::DocumentId>& expected) {
                SUBCASE(name) {
                    auto* gut = index.LookupNgram(name);
                    REQUIRE_NE(gut, nullptr);
                    CHECK_EQ(*gut, expected);
                }
            };

            check("Sea", {1});
            check("is ", {1});
            check("ing", {0, 1});
            check("abr", {0});
            check("aaa", {2});
        }

        SUBCASE("There is no garbage") {
            auto check = [&index](const char* name) {
                SUBCASE(name) {
                    auto* gut = index.LookupNgram(name);
                    REQUIRE_EQ(gut, nullptr);
                }
            };

            check("gar");
            check("arb");
            check("rba");
            check("bag");
            check("age");
            check("sea");
        }
    }
}
