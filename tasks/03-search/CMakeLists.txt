add_doctest(test_search test.cpp searcher.cpp index.cpp)

add_executable(bench_search bench.cpp searcher.cpp index.cpp)
target_link_libraries(bench_search is_utf8::is_utf8)
