#include "index.h"
#include "searcher.h"

#include <is_utf8.h>

#include <filesystem>
#include <iostream>
#include <fstream>
#include <sstream>


namespace codesearch::bench {

void AddFiles(IndexBuilder* builder, const char* dir) {
    for (auto&& entry : std::filesystem::recursive_directory_iterator{dir}) {
        if (!entry.is_regular_file()) {
            continue;
        }

        std::cerr << "Indexing file " << entry.path() << std::endl;

        std::ifstream f{entry.path()};
        std::stringstream buffer;
        buffer << f.rdbuf();

        const std::string& str = buffer.str();

        if (!is_utf8(str.data(), str.size())) {
            continue;
        }

        builder->AddDocument(Document{
            .path = entry.path().generic_string(),
            .text = str,
        });
    }
}

} // namespace codesearch::bench

auto now() {
    return std::chrono::high_resolution_clock::now();
}

double seconds(std::chrono::high_resolution_clock::duration d) {
    return std::chrono::duration_cast<std::chrono::duration<double>>(d).count();
}

int main(int argc, const char* argv[]) {
    auto before_index = now();

    std::cerr << "Start indexing" << std::endl;

    codesearch::IndexBuilder builder{codesearch::IndexOptions{.arity = 3}};

    for (int i = 1; i < argc; ++i) {
        codesearch::bench::AddFiles(&builder, argv[i]);
    }
    codesearch::Index index{std::move(builder).Finish()};

    auto after_index = now();

    std::cerr << "Indexing took " << seconds(after_index - before_index) << " seconds" << std::endl;

    codesearch::Searcher searcher{std::move(index)};

    double sumtime = 0;
    int numreq = 0;
    std::string line;
    while (std::getline(std::cin, line)) {
        ++numreq;

        auto before_search = now();

        int count = 0;
        searcher.Search(line, [&](const codesearch::Match&) {
            ++count;
        });
        std::cerr << "Found " << count << " matches" << std::endl;

        auto after_search = now();

        sumtime += seconds(after_search - before_search);
    }

    std::cout << "Average search time: " << sumtime / numreq << " seconds\n";
}
