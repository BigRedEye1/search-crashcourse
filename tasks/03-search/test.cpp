#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "../../contrib/doctest/doctest/doctest.h"

#include "index.h"
#include "searcher.h"


codesearch::Index BuildIndex(
    const std::vector<codesearch::Document>& docs,
    codesearch::IndexOptions options = {}
) {
    codesearch::IndexBuilder builder{options};

    for (auto&& doc : docs) {
        builder.AddDocument(doc);
    }

    return std::move(builder).Finish();
}

std::vector<codesearch::Match> CollectMatches(const codesearch::Searcher& searcher, std::string_view query) {
    std::vector<codesearch::Match> matches;
    searcher.Search(query, [&matches](codesearch::Match match){
        matches.push_back(std::move(match));
    });
    return matches;
}

void CheckSearcher(
    const codesearch::Searcher& searcher,
    const char* query,
    const std::vector<codesearch::Match>& expected
) {
    SUBCASE(query) {
        auto matches = CollectMatches(searcher, query);
        CHECK(matches == expected);
    }
}

TEST_SUITE("Searcher") {
    TEST_CASE("Simple") {
        auto index = BuildIndex({{
            .path = "1.txt",
            .text = R"(foo
bar)",
        }, {
            .path = "2.txt",
            .text = R"(aa)",
        }, {
            .path = "3.txt",
            .text = R"(kek kek)"
        }});

        codesearch::Searcher searcher{std::move(index)};

        std::unordered_map<const char*, std::vector<codesearch::Match>> expected;

        expected["foo"] = {
            {.file = "1.txt", .line = 0, .column = 0, .snippet = "foo", },
        };

        expected["bar"] = {
            {.file = "1.txt", .line = 1, .column = 0, .snippet = "bar", },
        };

        expected["hehe"] = {
        };

        expected["aa"] = {
            {.file = "2.txt", .line = 0, .column = 0, .snippet = "aa", },
        };

        expected["kek"] = {
            {.file = "3.txt", .line = 0, .column = 0, .snippet = "kek kek", },
            {.file = "3.txt", .line = 0, .column = 4, .snippet = "kek kek", },
        };

        for (auto&& [k, v] : expected) {
            CheckSearcher(searcher, k, v);
        }
    }

    TEST_CASE("Lorem ipsum") {
        auto index = BuildIndex({{
            .path = "1.txt",
            .text = R"(Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Aenean malesuada felis non tellus lobortis, a vehicula risus blandit.
Sed tincidunt neque justo, sed porta dui dignissim quis.
Pellentesque fringilla justo dolor, et scelerisque ex placerat sed.
Curabitur id turpis nibh.
In rhoncus non odio quis iaculis.
Nunc libero neque, faucibus nec dignissim et, auctor quis augue.
Nam lacinia porttitor diam, non dignissim ligula sollicitudin eu.
Cras sed ex nibh.
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;)",
        }, {
            .path = "2.txt",
            .text = R"(Pellentesque non ante eu augue sollicitudin commodo nec nec nisi.
Mauris felis odio, tempus mattis ultricies vel, sodales eu lorem.
Proin consectetur dictum faucibus.
In pharetra ultrices congue.
Quisque nec ullamcorper risus.
Morbi diam enim, lobortis vitae nulla ac, molestie sollicitudin lectus.
Maecenas nec felis at justo malesuada egestas.
Vivamus ultrices erat sed velit lobortis, et laoreet nisi pulvinar.
In tincidunt, quam sed accumsan rhoncus, quam enim tristique magna, vitae porta diam lacus dignissim nisi.
Aenean posuere, lectus vel porttitor convallis, nulla nunc finibus lectus, et fermentum sem ante sit amet nibh.
Phasellus quis consectetur metus.
Phasellus suscipit sem non magna porttitor commodo.
Duis elementum pharetra quam.)",
        }, {
            .path = "3.txt",
            .text = R"(Suspendisse eget condimentum nisl, at sagittis ex.
Phasellus sit amet commodo quam, in pharetra justo.
Quisque scelerisque commodo ex vel elementum.
Duis lobortis non augue sed bibendum.
Donec nunc massa, sagittis sed sem a, porta porta dolor.
Etiam quis sapien ut justo ultrices sodales quis quis mauris.
Vivamus lacinia mi ac diam imperdiet commodo.
Phasellus quis condimentum leo, pellentesque volutpat nulla.
Duis dictum vel sapien vel ultrices.
Praesent faucibus leo tincidunt nisl eleifend bibendum.
Suspendisse potenti.
Curabitur mollis, mauris et lacinia semper, lorem est aliquet mi, a aliquet velit est nec nisi.
Morbi convallis ex eu diam aliquet ultricies eu vel felis.
Vestibulum id tempor metus.
Sed commodo, erat vel scelerisque elementum, risus risus semper sem, porta malesuada urna ante at urna.
Nulla blandit urna tellus, eu mattis orci elementum vel.)"
        }});

        codesearch::Searcher searcher{std::move(index)};

        std::unordered_map<const char*, std::vector<codesearch::Match>> expected;

        expected["risus"] = {
            {.file = "1.txt", .line = 1, .column = 55, .snippet = "Aenean malesuada felis non tellus lobortis, a vehicula risus blandit.", },
            {.file = "2.txt", .line = 4, .column = 24, .snippet = "Quisque nec ullamcorper risus.", },
            {.file = "3.txt", .line = 14, .column = 45, .snippet = "Sed commodo, erat vel scelerisque elementum, risus risus semper sem, porta malesuada urna ante at urna.", },
            {.file = "3.txt", .line = 14, .column = 51, .snippet = "Sed commodo, erat vel scelerisque elementum, risus risus semper sem, porta malesuada urna ante at urna.", },
        };

        expected["dolor sit amet"] = {
            {.file = "1.txt", .line = 0, .column = 12, .snippet = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", },
        };

        expected["nisl"] = {
            {.file = "3.txt", .line = 0, .column = 29, .snippet = "Suspendisse eget condimentum nisl, at sagittis ex.", },
            {.file = "3.txt", .line = 9, .column = 32, .snippet = "Praesent faucibus leo tincidunt nisl eleifend bibendum.", },
        };

        expected["urna tellus"] = {
            {.file = "3.txt", .line = 15, .column = 14, .snippet = "Nulla blandit urna tellus, eu mattis orci elementum vel.", },
        };

        expected["in pharetra"] = {
            {.file = "3.txt", .line = 1, .column = 33, .snippet = "Phasellus sit amet commodo quam, in pharetra justo.", },
        };

        for (auto&& [k, v] : expected) {
            CheckSearcher(searcher, k, v);
        }
    }
}
