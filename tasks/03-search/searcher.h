#pragma once

#include "index.h"

#include <ostream>


namespace codesearch {

////////////////////////////////////////////////////////////////////////////////

struct Match {
    std::string file;
    size_t line = 0;
    size_t column = 0;
    std::string snippet;

    bool operator==(const Match& match) const noexcept {
        return std::tie(match.file, match.line, match.column, match.snippet) == std::tie(file, line, column, snippet);
    }

    bool operator!=(const Match& match) const noexcept {
        return !operator==(match);
    }
};

inline std::ostream& operator<<(std::ostream& os, const Match& match) {
    return os << '{' << match.file << ':' << match.line << ':' << match.column << ':' << match.snippet << '}';
}

////////////////////////////////////////////////////////////////////////////////

class Searcher {
public:
    using MatchCallback = std::function<void(Match)>;

public:
    explicit Searcher(Index index) {
        // Your code goes here
    }

    void Search(std::string_view query, const MatchCallback& callback) const {
        // Your code goes here
    }

private:
    // First step of the search process.
    // Generate vector of documents that potentially contain matching line.
    std::vector<DocumentId> GenerateCandidates(std::string_view query) const {
        // Your code goes here
    }

    // Second step of the search process.
    // Find all the matching documents.
    void ProcessCandidate(std::string_view query, DocumentId id, const MatchCallback& callback) {
        // Your code goes here
    }

private:
    // Your code goes here
};

////////////////////////////////////////////////////////////////////////////////

} // namespace codesearch
